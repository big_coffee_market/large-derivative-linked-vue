Web地址:  http://81.68.131.83:81/login?redirect=%2Findex


博客地址： https://blog.csdn.net/qq_30776389/article/details/127161479?spm=1001.2014.3001.5502
## 开发

```bash
# 克隆项目
git clone https://gitee.com/y_project/RuoYi-Vue

# 进入项目目录
cd ruoyi-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 指令加载模型


![输入图片说明](https://img-blog.csdnimg.cn/450ea7a71dc64c168e7ada9d4ee1ef6b.png "屏幕截图.png")


![输入图片说明](https://img-blog.csdnimg.cn/aab9ddfe75e849fbb89a705dff579ba5.png "屏幕截图.png")


![输入图片说明](https://img-blog.csdnimg.cn/3d7e3af323184b818e489443d273352a.png "屏幕截图.png")


![输入图片说明](https://img-blog.csdnimg.cn/b504e0b9517b4b49b1b7308dd28f60c1.png "屏幕截图.png")
