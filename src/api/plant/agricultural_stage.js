import request from '@/utils/request'

// 查询农事阶段列表
export function listAgricultural_stage(query) {
  return request({
    url: '/plant/agricultural_stage/list',
    method: 'get',
    params: query
  })
}

// 查询农事阶段详细
export function getAgricultural_stage(id) {
  return request({
    url: '/plant/agricultural_stage/' + id,
    method: 'get'
  })
}

// 新增农事阶段
export function addAgricultural_stage(data) {
  return request({
    url: '/plant/agricultural_stage',
    method: 'post',
    data: data
  })
}

// 修改农事阶段
export function updateAgricultural_stage(data) {
  return request({
    url: '/plant/agricultural_stage',
    method: 'put',
    data: data
  })
}

// 删除农事阶段
export function delAgricultural_stage(id) {
  return request({
    url: '/plant/agricultural_stage/' + id,
    method: 'delete'
  })
}
