import request from '@/utils/request'

// 查询产品加工列表
export function listProduct_processing(query) {
  return request({
    url: '/plant/product_processing/list',
    method: 'get',
    params: query
  })
}

// 查询产品加工详细
export function getProduct_processing(id) {
  return request({
    url: '/plant/product_processing/' + id,
    method: 'get'
  })
}

// 新增产品加工
export function addProduct_processing(data) {
  return request({
    url: '/plant/product_processing',
    method: 'post',
    data: data
  })
}

// 修改产品加工
export function updateProduct_processing(data) {
  return request({
    url: '/plant/product_processing',
    method: 'put',
    data: data
  })
}

// 删除产品加工
export function delProduct_processing(id) {
  return request({
    url: '/plant/product_processing/' + id,
    method: 'delete'
  })
}
