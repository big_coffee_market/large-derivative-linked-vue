import request from '@/utils/request'

// 查询农事计划列表
export function listAgricultural_plan(query) {
  return request({
    url: '/plant/agricultural_plan/list',
    method: 'get',
    params: query
  })
}

// 查询农事计划详细
export function getAgricultural_plan(id) {
  return request({
    url: '/plant/agricultural_plan/' + id,
    method: 'get'
  })
}

// 新增农事计划
export function addAgricultural_plan(data) {
  return request({
    url: '/plant/agricultural_plan',
    method: 'post',
    data: data
  })
}

// 修改农事计划
export function updateAgricultural_plan(data) {
  return request({
    url: '/plant/agricultural_plan',
    method: 'put',
    data: data
  })
}

// 删除农事计划
export function delAgricultural_plan(id) {
  return request({
    url: '/plant/agricultural_plan/' + id,
    method: 'delete'
  })
}
