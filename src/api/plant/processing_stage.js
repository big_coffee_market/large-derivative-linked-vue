import request from '@/utils/request'

// 查询加工类型列表
export function listProcessing_stage(query) {
  return request({
    url: '/plant/processing_stage/list',
    method: 'get',
    params: query
  })
}

// 查询加工类型详细
export function getProcessing_stage(id) {
  return request({
    url: '/plant/processing_stage/' + id,
    method: 'get'
  })
}

// 新增加工类型
export function addProcessing_stage(data) {
  return request({
    url: '/plant/processing_stage',
    method: 'post',
    data: data
  })
}

// 修改加工类型
export function updateProcessing_stage(data) {
  return request({
    url: '/plant/processing_stage',
    method: 'put',
    data: data
  })
}

// 删除加工类型
export function delProcessing_stage(id) {
  return request({
    url: '/plant/processing_stage/' + id,
    method: 'delete'
  })
}
