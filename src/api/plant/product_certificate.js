import request from '@/utils/request'

// 查询产品证书列表
export function listProduct_certificate(query) {
  return request({
    url: '/plant/product_certificate/list',
    method: 'get',
    params: query
  })
}

// 查询产品证书详细
export function getProduct_certificate(id) {
  return request({
    url: '/plant/product_certificate/' + id,
    method: 'get'
  })
}

// 新增产品证书
export function addProduct_certificate(data) {
  return request({
    url: '/plant/product_certificate',
    method: 'post',
    data: data
  })
}

// 修改产品证书
export function updateProduct_certificate(data) {
  return request({
    url: '/plant/product_certificate',
    method: 'put',
    data: data
  })
}

// 删除产品证书
export function delProduct_certificate(id) {
  return request({
    url: '/plant/product_certificate/' + id,
    method: 'delete'
  })
}
