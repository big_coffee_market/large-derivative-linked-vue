import request from '@/utils/request'

// 查询采收管理列表
export function listAgricultural_harvest(query) {
  return request({
    url: '/plant/agricultural_harvest/list',
    method: 'get',
    params: query
  })
}

// 查询采收管理详细
export function getAgricultural_harvest(id) {
  return request({
    url: '/plant/agricultural_harvest/' + id,
    method: 'get'
  })
}

// 新增采收管理
export function addAgricultural_harvest(data) {
  return request({
    url: '/plant/agricultural_harvest',
    method: 'post',
    data: data
  })
}

// 修改采收管理
export function updateAgricultural_harvest(data) {
  return request({
    url: '/plant/agricultural_harvest',
    method: 'put',
    data: data
  })
}

// 删除采收管理
export function delAgricultural_harvest(id) {
  return request({
    url: '/plant/agricultural_harvest/' + id,
    method: 'delete'
  })
}
