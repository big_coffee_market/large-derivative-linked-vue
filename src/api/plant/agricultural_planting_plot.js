import request from '@/utils/request'

// 查询种植地块列表
export function listAgricultural_planting_plot(query) {
  return request({
    url: '/plant/agricultural_planting_plot/list',
    method: 'get',
    params: query
  })
}

// 查询种植地块详细
export function getAgricultural_planting_plot(id) {
  return request({
    url: '/plant/agricultural_planting_plot/' + id,
    method: 'get'
  })
}

// 新增种植地块
export function addAgricultural_planting_plot(data) {
  return request({
    url: '/plant/agricultural_planting_plot',
    method: 'post',
    data: data
  })
}

// 修改种植地块
export function updateAgricultural_planting_plot(data) {
  return request({
    url: '/plant/agricultural_planting_plot',
    method: 'put',
    data: data
  })
}

// 删除种植地块
export function delAgricultural_planting_plot(id) {
  return request({
    url: '/plant/agricultural_planting_plot/' + id,
    method: 'delete'
  })
}
