import request from '@/utils/request'

// 查询证书类型列表
export function listCertificate_stage(query) {
  return request({
    url: '/plant/certificate_stage/list',
    method: 'get',
    params: query
  })
}

// 查询证书类型详细
export function getCertificate_stage(id) {
  return request({
    url: '/plant/certificate_stage/' + id,
    method: 'get'
  })
}

// 新增证书类型
export function addCertificate_stage(data) {
  return request({
    url: '/plant/certificate_stage',
    method: 'post',
    data: data
  })
}

// 修改证书类型
export function updateCertificate_stage(data) {
  return request({
    url: '/plant/certificate_stage',
    method: 'put',
    data: data
  })
}

// 删除证书类型
export function delCertificate_stage(id) {
  return request({
    url: '/plant/certificate_stage/' + id,
    method: 'delete'
  })
}
