import request from '@/utils/request'

// 查询产品检测列表
export function listProduct_testing(query) {
  return request({
    url: '/plant/product_testing/list',
    method: 'get',
    params: query
  })
}

// 查询产品检测详细
export function getProduct_testing(id) {
  return request({
    url: '/plant/product_testing/' + id,
    method: 'get'
  })
}

// 新增产品检测
export function addProduct_testing(data) {
  return request({
    url: '/plant/product_testing',
    method: 'post',
    data: data
  })
}

// 修改产品检测
export function updateProduct_testing(data) {
  return request({
    url: '/plant/product_testing',
    method: 'put',
    data: data
  })
}

// 删除产品检测
export function delProduct_testing(id) {
  return request({
    url: '/plant/product_testing/' + id,
    method: 'delete'
  })
}
