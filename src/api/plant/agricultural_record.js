import request from '@/utils/request'

// 查询农事记录列表
export function listAgricultural_record(query) {
  return request({
    url: '/plant/agricultural_record/list',
    method: 'get',
    params: query
  })
}

// 查询农事记录详细
export function getAgricultural_record(id) {
  return request({
    url: '/plant/agricultural_record/' + id,
    method: 'get'
  })
}

// 新增农事记录
export function addAgricultural_record(data) {
  return request({
    url: '/plant/agricultural_record',
    method: 'post',
    data: data
  })
}

// 修改农事记录
export function updateAgricultural_record(data) {
  return request({
    url: '/plant/agricultural_record',
    method: 'put',
    data: data
  })
}

// 删除农事记录
export function delAgricultural_record(id) {
  return request({
    url: '/plant/agricultural_record/' + id,
    method: 'delete'
  })
}
