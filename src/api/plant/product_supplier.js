import request from '@/utils/request'

// 查询供货商列表
export function listProduct_supplier(query) {
  return request({
    url: '/plant/product_supplier/list',
    method: 'get',
    params: query
  })
}

// 查询供货商详细
export function getProduct_supplier(id) {
  return request({
    url: '/plant/product_supplier/' + id,
    method: 'get'
  })
}

// 新增供货商
export function addProduct_supplier(data) {
  return request({
    url: '/plant/product_supplier',
    method: 'post',
    data: data
  })
}

// 修改供货商
export function updateProduct_supplier(data) {
  return request({
    url: '/plant/product_supplier',
    method: 'put',
    data: data
  })
}

// 删除供货商
export function delProduct_supplier(id) {
  return request({
    url: '/plant/product_supplier/' + id,
    method: 'delete'
  })
}
