import request from '@/utils/request'

// 查询农作物列表
export function listAgricultural_crop(query) {
  return request({
    url: '/plant/agricultural_crop/list',
    method: 'get',
    params: query
  })
}

// 查询农作物详细
export function getAgricultural_crop(id) {
  return request({
    url: '/plant/agricultural_crop/' + id,
    method: 'get'
  })
}

// 新增农作物
export function addAgricultural_crop(data) {
  return request({
    url: '/plant/agricultural_crop',
    method: 'post',
    data: data
  })
}

// 修改农作物
export function updateAgricultural_crop(data) {
  return request({
    url: '/plant/agricultural_crop',
    method: 'put',
    data: data
  })
}

// 删除农作物
export function delAgricultural_crop(id) {
  return request({
    url: '/plant/agricultural_crop/' + id,
    method: 'delete'
  })
}
