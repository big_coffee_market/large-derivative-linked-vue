import request from '@/utils/request'

// 查询检测类型列表
export function listProduct_test_stage(query) {
  return request({
    url: '/plant/product_test_stage/list',
    method: 'get',
    params: query
  })
}

// 查询检测类型详细
export function getProduct_test_stage(id) {
  return request({
    url: '/plant/product_test_stage/' + id,
    method: 'get'
  })
}

// 新增检测类型
export function addProduct_test_stage(data) {
  return request({
    url: '/plant/product_test_stage',
    method: 'post',
    data: data
  })
}

// 修改检测类型
export function updateProduct_test_stage(data) {
  return request({
    url: '/plant/product_test_stage',
    method: 'put',
    data: data
  })
}

// 删除检测类型
export function delProduct_test_stage(id) {
  return request({
    url: '/plant/product_test_stage/' + id,
    method: 'delete'
  })
}
