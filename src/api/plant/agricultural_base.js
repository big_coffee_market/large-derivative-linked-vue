import request from '@/utils/request'

// 查询基地列表
export function listAgricultural_base(query) {
  return request({
    url: '/plant/agricultural_base/list',
    method: 'get',
    params: query
  })
}

// 查询基地详细
export function getAgricultural_base(id) {
  return request({
    url: '/plant/agricultural_base/' + id,
    method: 'get'
  })
}

// 新增基地
export function addAgricultural_base(data) {
  return request({
    url: '/plant/agricultural_base',
    method: 'post',
    data: data
  })
}

// 修改基地
export function updateAgricultural_base(data) {
  return request({
    url: '/plant/agricultural_base',
    method: 'put',
    data: data
  })
}

// 删除基地
export function delAgricultural_base(id) {
  return request({
    url: '/plant/agricultural_base/' + id,
    method: 'delete'
  })
}
