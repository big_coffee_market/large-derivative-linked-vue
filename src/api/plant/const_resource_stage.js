import request from '@/utils/request'

// 查询农资类型列表
export function listConst_resource_stage(query) {
  return request({
    url: '/plant/const_resource_stage/list',
    method: 'get',
    params: query
  })
}

// 查询农资类型详细
export function getConst_resource_stage(id) {
  return request({
    url: '/plant/const_resource_stage/' + id,
    method: 'get'
  })
}

// 新增农资类型
export function addConst_resource_stage(data) {
  return request({
    url: '/plant/const_resource_stage',
    method: 'post',
    data: data
  })
}

// 修改农资类型
export function updateConst_resource_stage(data) {
  return request({
    url: '/plant/const_resource_stage',
    method: 'put',
    data: data
  })
}

// 删除农资类型
export function delConst_resource_stage(id) {
  return request({
    url: '/plant/const_resource_stage/' + id,
    method: 'delete'
  })
}
