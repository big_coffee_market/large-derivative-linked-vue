import request from '@/utils/request'

// 查询生长阶段列表
export function listCrop_stage(query) {
  return request({
    url: '/plant/crop_stage/list',
    method: 'get',
    params: query
  })
}

// 查询生长阶段详细
export function getCrop_stage(id) {
  return request({
    url: '/plant/crop_stage/' + id,
    method: 'get'
  })
}

// 新增生长阶段
export function addCrop_stage(data) {
  return request({
    url: '/plant/crop_stage',
    method: 'post',
    data: data
  })
}

// 修改生长阶段
export function updateCrop_stage(data) {
  return request({
    url: '/plant/crop_stage',
    method: 'put',
    data: data
  })
}

// 删除生长阶段
export function delCrop_stage(id) {
  return request({
    url: '/plant/crop_stage/' + id,
    method: 'delete'
  })
}
