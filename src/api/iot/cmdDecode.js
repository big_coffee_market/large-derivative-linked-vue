import request from '@/utils/request'

// 查询指令解析列表
export function listCmdDecode(query) {
  return request({
    url: '/iot/cmdDecode/list',
    method: 'get',
    params: query
  })
}

// 查询指令解析详细
export function getCmdDecode(id) {
  return request({
    url: '/iot/cmdDecode/' + id,
    method: 'get'
  })
}

// 新增指令解析
export function addCmdDecode(data) {
  return request({
    url: '/iot/cmdDecode',
    method: 'post',
    data: data
  })
}

// 修改指令解析
export function updateCmdDecode(data) {
  return request({
    url: '/iot/cmdDecode',
    method: 'put',
    data: data
  })
}

// 删除指令解析
export function delCmdDecode(id) {
  return request({
    url: '/iot/cmdDecode/' + id,
    method: 'delete'
  })
}
