import request from '@/utils/request'

// 查询指令列表
export function listCmd(query) {
  return request({
    url: '/iot/cmd/list',
    method: 'get',
    params: query
  })
}

// 查询指令详细
export function getCmd(id) {
  return request({
    url: '/iot/cmd/' + id,
    method: 'get'
  })
}

// 查询对应的指令解释 explain/{id}
export function getCmdExplain(id) {
  return request({
    url: '/iot/cmd/explain/' + id,
    method: 'get'
  })
}

//cmd组装指令
export function cmdEncodeJson(data) {
  return request({
    url: '/iot/cmd/generator/encode/json',
    method: 'post',
    data: data
  })
}

//cmd组装指令
export function cmdDecodeJson(data) {
  return request({
    url: '/iot/cmd/generator/decode/json',
    method: 'post',
    data: data
  })
}

// 新增指令
export function addCmd(data) {
  return request({
    url: '/iot/cmd',
    method: 'post',
    data: data
  })
}

// 修改指令
export function updateCmd(data) {
  return request({
    url: '/iot/cmd',
    method: 'put',
    data: data
  })
}

// 删除指令
export function delCmd(id) {
  return request({
    url: '/iot/cmd/' + id,
    method: 'delete'
  })
}
