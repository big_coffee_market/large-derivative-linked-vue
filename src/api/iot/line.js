import request from '@/utils/request'

// 查询设备离在线记录列表
export function listLine(query) {
  return request({
    url: '/iot/line/list',
    method: 'get',
    params: query
  })
}

// 查询设备离在线记录详细
export function getLine(id) {
  return request({
    url: '/iot/line/' + id,
    method: 'get'
  })
}

// 新增设备离在线记录
export function addLine(data) {
  return request({
    url: '/iot/line',
    method: 'post',
    data: data
  })
}

// 修改设备离在线记录
export function updateLine(data) {
  return request({
    url: '/iot/line',
    method: 'put',
    data: data
  })
}

// 删除设备离在线记录
export function delLine(id) {
  return request({
    url: '/iot/line/' + id,
    method: 'delete'
  })
}
