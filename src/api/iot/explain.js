import request from '@/utils/request'

// 查询指令解释列表
export function listExplain(query) {
  return request({
    url: '/iot/explain/list',
    method: 'get',
    params: query
  })
}

// 查询指令解释详细
export function getExplain(id) {
  return request({
    url: '/iot/explain/' + id,
    method: 'get'
  })
}

// 新增指令解释
export function addExplain(data) {
  return request({
    url: '/iot/explain',
    method: 'post',
    data: data
  })
}

// 修改指令解释
export function updateExplain(data) {
  return request({
    url: '/iot/explain',
    method: 'put',
    data: data
  })
}

// 删除指令解释
export function delExplain(id) {
  return request({
    url: '/iot/explain/' + id,
    method: 'delete'
  })
}

//指令翻译
export function  translateExplain(data) {
  return request({
    url: '/iot/explain/translate',
    method: 'post',
    data: data
  })
}
