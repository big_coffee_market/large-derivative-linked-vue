import request from '@/utils/request'

// 查询厂家协议列表
export function listFrame(query) {
  return request({
    url: '/iot/frame/list',
    method: 'get',
    params: query
  })
}

// 查询厂家协议详细
export function getFrame(id) {
  return request({
    url: '/iot/frame/' + id,
    method: 'get'
  })
}

// 新增厂家协议
export function addFrame(data) {
  return request({
    url: '/iot/frame',
    method: 'post',
    data: data
  })
}

// 修改厂家协议
export function updateFrame(data) {
  return request({
    url: '/iot/frame',
    method: 'put',
    data: data
  })
}

// 删除厂家协议
export function delFrame(id) {
  return request({
    url: '/iot/frame/' + id,
    method: 'delete'
  })
}
