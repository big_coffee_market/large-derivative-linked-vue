import request from '@/utils/request'

// 查询设备列表
export function listDevice(query) {
  return request({
    url: '/iot/device/list',
    method: 'get',
    params: query
  })
}

// 查询设备详细
export function getDevice(id) {
  return request({
    url: '/iot/device/' + id,
    method: 'get'
  })
}

// 新增设备
export function addDevice(data) {
  return request({
    url: '/iot/device',
    method: 'post',
    data: data
  })
}

// 修改设备
export function updateDevice(data) {
  return request({
    url: '/iot/device',
    method: 'put',
    data: data
  })
}

// 删除设备
export function delDevice(id) {
  return request({
    url: '/iot/device/' + id,
    method: 'delete'
  })
}

// 查询设备对应的指令json详细
export function getDeviceCmdExplain(id) {
  return request({
    url: '/iot/device/cmd/explain/' + id,
    method: 'get'
  })
}

//下发指令对应的数据
export  function  deviceOperate(data){
  return request({
    url: '/iot/device/cmd/operate',
    method: 'post',
    data: data
  })
}

//设备指令交互
export function deviceCmdTranslate(data){
  return request({
    url: "/iot/device/cmd/translate",
    method: 'post',
    data: data
  })
}

