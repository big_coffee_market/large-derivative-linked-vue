import request from '@/utils/request'

// 查询产品-属性列表
export function listField(query) {
  return request({
    url: '/iot/field/list',
    method: 'get',
    params: query
  })
}

// 查询产品-属性详细
export function getField(id) {
  return request({
    url: '/iot/field/' + id,
    method: 'get'
  })
}

// 新增产品-属性
export function addField(data) {
  return request({
    url: '/iot/field',
    method: 'post',
    data: data
  })
}

// 修改产品-属性
export function updateField(data) {
  return request({
    url: '/iot/field',
    method: 'put',
    data: data
  })
}

// 删除产品-属性
export function delField(id) {
  return request({
    url: '/iot/field/' + id,
    method: 'delete'
  })
}
