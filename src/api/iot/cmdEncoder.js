import request from '@/utils/request'

// 查询指令编码列表
export function listCmdEncoder(query) {
  return request({
    url: '/iot/cmdEncoder/list',
    method: 'get',
    params: query
  })
}

// 查询指令编码详细
export function getCmdEncoder(id) {
  return request({
    url: '/iot/cmdEncoder/' + id,
    method: 'get'
  })
}

// 新增指令编码
export function addCmdEncoder(data) {
  return request({
    url: '/iot/cmdEncoder',
    method: 'post',
    data: data
  })
}

// 修改指令编码
export function updateCmdEncoder(data) {
  return request({
    url: '/iot/cmdEncoder',
    method: 'put',
    data: data
  })
}

// 删除指令编码
export function delCmdEncoder(id) {
  return request({
    url: '/iot/cmdEncoder/' + id,
    method: 'delete'
  })
}
