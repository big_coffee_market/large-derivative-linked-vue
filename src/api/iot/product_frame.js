import request from '@/utils/request'

// 查询产品-数据帧列表
export function listProduct_frame(query) {
  return request({
    url: '/iot/product_frame/list',
    method: 'get',
    params: query
  })
}

// 查询产品-数据帧详细
export function getProduct_frame(id) {
  return request({
    url: '/iot/product_frame/' + id,
    method: 'get'
  })
}

// 新增产品-数据帧
export function addProduct_frame(data) {
  return request({
    url: '/iot/product_frame',
    method: 'post',
    data: data
  })
}

// 修改产品-数据帧
export function updateProduct_frame(data) {
  return request({
    url: '/iot/product_frame',
    method: 'put',
    data: data
  })
}

// 删除产品-数据帧
export function delProduct_frame(id) {
  return request({
    url: '/iot/product_frame/' + id,
    method: 'delete'
  })
}
