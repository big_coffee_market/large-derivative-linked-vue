import request from '@/utils/request'

// 查询平台信息列表
export function listPlatform(query) {
  return request({
    url: '/iot/platform/list',
    method: 'get',
    params: query
  })
}

// 查询平台信息详细
export function getPlatform(id) {
  return request({
    url: '/iot/platform/' + id,
    method: 'get'
  })
}

// 新增平台信息
export function addPlatform(data) {
  return request({
    url: '/iot/platform',
    method: 'post',
    data: data
  })
}

// 修改平台信息
export function updatePlatform(data) {
  return request({
    url: '/iot/platform',
    method: 'put',
    data: data
  })
}

// 删除平台信息
export function delPlatform(id) {
  return request({
    url: '/iot/platform/' + id,
    method: 'delete'
  })
}
