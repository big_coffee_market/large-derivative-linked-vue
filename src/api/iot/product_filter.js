import request from '@/utils/request'

// 查询数据筛选列表
export function listProduct_filter(query) {
  return request({
    url: '/iot/product_filter/list',
    method: 'get',
    params: query
  })
}

// 查询数据筛选详细
export function getProduct_filter(id) {
  return request({
    url: '/iot/product_filter/' + id,
    method: 'get'
  })
}

// 新增数据筛选
export function addProduct_filter(data) {
  return request({
    url: '/iot/product_filter',
    method: 'post',
    data: data
  })
}

// 修改数据筛选
export function updateProduct_filter(data) {
  return request({
    url: '/iot/product_filter',
    method: 'put',
    data: data
  })
}

// 删除数据筛选
export function delProduct_filter(id) {
  return request({
    url: '/iot/product_filter/' + id,
    method: 'delete'
  })
}

//获取树形结构
export function getProduct_tree() {
  return request({
    url: '/iot/product_filter/tree',
    method: 'get'
  })

}
