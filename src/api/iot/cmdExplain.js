import request from '@/utils/request'

// 查询指令解释列表
export function listCmdExplain(query) {
  return request({
    url: '/iot/cmdExplain/list',
    method: 'get',
    params: query
  })
}

// 查询指令解释详细
export function getCmdExplain(id) {
  return request({
    url: '/iot/cmdExplain/' + id,
    method: 'get'
  })
}

// 新增指令解释
export function addCmdExplain(data) {
  return request({
    url: '/iot/cmdExplain',
    method: 'post',
    data: data
  })
}

// 修改指令解释
export function updateCmdExplain(data) {
  return request({
    url: '/iot/cmdExplain',
    method: 'put',
    data: data
  })
}

// 删除指令解释
export function delCmdExplain(id) {
  return request({
    url: '/iot/cmdExplain/' + id,
    method: 'delete'
  })
}
